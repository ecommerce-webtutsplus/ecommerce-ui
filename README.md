# ecommerce-ui-product-category
```
The base URL of API is present in App.vue
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
or
npm run serve -- --port 8003
```

### Compiles and minifies for production
```
npm run build
```

###login in digital ocene

ssh root@167.99.37.15

(use password: webTuts9plus)

cd ecommere-ui
sh run.sh

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
